function htmlEscape(text) {
  const p = document.createElement("p");
  p.innerText = text;
  return p.innerHTML;
}


function copy(text, e) {

  if(!navigator.clipboard) {
    const $e = $(e);
    const oldTitle = $e.attr('data-bs-original-title');
    $e.attr('data-bs-original-title', "ERROR: Could not write to clipboard").tooltip('show');
    $e.attr('data-bs-original-title', oldTitle);
    return;
  }

  navigator.clipboard.writeText(text).then(function() {

    const $e = $(e);
    const oldTitle = $e.attr('data-bs-original-title');
    $e.attr('data-bs-original-title', "Path copied to clipboard").tooltip('show');
    $e.attr('data-bs-original-title', oldTitle);


    Swal.fire({
      icon: 'success',
      title: 'Path copied to clipboard',
      html: `<span style="font-size: 0.7em">${text}</span>`,
      toast: true,
      position: 'top-end',
      showConfirmButton: false,
      timer: 2500,
      timerProgressBar: true
    })


  }, function(err) {
    console.error('Async: Could not copy text: ', err);
  });
}
