//https://github.com/dropzone/dropzone

$("#uploaddropzone").dropzone({
  dictDefaultMessage: 'Drop files here or click to select files.',
  parallelUploads: 4,
  maxFilesize: 2048,
  queuecomplete: () => {

    const { files } = window.Dropzone.instances[0];
    const success = files.filter(f => f.status === 'success');
    const error = files.filter(f => f.status === 'error');

    const title = success.length === 1 ? success.length + ' file uploaded' : success.length + ' files uploaded';
    let text = error.length === 1 ? error.length + ' file with error' : error.length + ' files with error';
    let hasErrors = error.length > 0;
    let confirmButtonColor = '';

    if(!hasErrors) {
      text = '';
      confirmButtonColor = '#198754';
    }
    else{
      confirmButtonColor = '#dc3545'
      cancelButtonColor = '#198754';
    }

    Swal.fire({
      title,
      text,
      showCancelButton: hasErrors ? true : false,
      cancelButtonText: hasErrors ? 'Refresh' : 'Cancel',
      confirmButtonColor,
      confirmButtonText: hasErrors ? 'OK' : 'Refresh'
    }).then(res => {
      if(res.isConfirmed && !hasErrors) {
        window.location.reload();
      }
      else if (res.isDismissed && hasErrors){
        window.location.reload();
      }
    })
  }
});