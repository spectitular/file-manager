$("#mkdir.modal").on("shown.bs.modal", function (e) {
  $("#mkdir-folder").focus();
});

function humanFileSize(size) {
  const i = size == 0 ? 0 : Math.floor(Math.log(size) / Math.log(1024));
  return (size / Math.pow(1024, i)).toFixed(2) * 1 + ' ' + ['B', 'kB', 'MB', 'GB', 'TB'][i];
}

$("#info.modal").on("shown.bs.modal", function (e) {
  let url = "/info.json";
  if (window.location.pathname.indexOf("/file-manager") > -1) {
    url = "/file-manager/info.json";
  }

  $.ajax({
    type: "GET",
    url: url,
    dataType: "json",
    success: function (data) {
      console.log(data);
      const fields = ["version", "hostname", "uptime", "base", "product"];
      fields.forEach((field) => {
        if (data && field && data[field]) {
          $("#" + field).text(data[field]);
        }
      });
      $("#free").text(humanFileSize(data.diskSpace.free));
      $("#size").text(humanFileSize(data.diskSpace.size));
    },
  });
});

$("#copyfilenames.modal").on("shown.bs.modal", function (e) {

  let url = new URL(window.location.href);
  const slash = url.pathname.endsWith('/') ? '' : '/';
  url.pathname += slash + 'list.json';


  const copyBox = document.querySelector('#copy-box');
  $.ajax({
    type: "GET",
    url: url.href,
    dataType: "json",
    success: function (data) {
      data.forEach(file => {
        if(file.name && file.name[0] !== '.' && file.fullpath) {
          copyBox.value += '/' + file.fullpath + '\n';
        }
      })
    },
  });
});


$("#upload.modal").on("hide.bs.modal", function (e) {
  window.location.reload();
});

$("#download.modal").on("shown.bs.modal", function (e) {
  console.log('download opened');
  let prefix = document.querySelector('.breadcrumb').innerText.replace(/\n/g,'_');
  prefix = prefix.replace('data_','');
  let list = JSON.parse(document.querySelector('#download .multi-files-value').value)
  const downloadFilename = prefix + '_' + list.join('__')+ '.zip'
  $('#download-zipname').val(downloadFilename);
});
