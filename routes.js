const favicon = require("serve-favicon");
const path = require("path");
const express = require("express");
const session = require("express-session");
const flash = require("connect-flash");
const bodyparser = require("body-parser");
const fileupload = require("express-fileupload");
module.exports = (app) => {
  app.use(favicon(path.join(__dirname, "assets", "favicon.ico")));

  app.use("/@assets", express.static(path.join(__dirname, "assets")));
  app.use(
    "/@assets/bootstrap",
    express.static(path.join(__dirname, "node_modules/bootstrap/dist"))
  );
  app.use(
    "/@assets/octicons",
    express.static(path.join(__dirname, "node_modules/@primer/octicons/build"))
  );
  app.use(
    "/@assets/jquery",
    express.static(path.join(__dirname, "node_modules/jquery/dist"))
  );
  app.use(
    "/@assets/filesize",
    express.static(path.join(__dirname, "node_modules/filesize/lib"))
  );
  app.use(
    "/@assets/dropzone",
    express.static(path.join(__dirname, "node_modules/dropzone/dist"))
  );
  app.use(
    session({
      resave: false,
      saveUninitialized: false,
      secret: process.env.SESSION_KEY || "meowmeow-or-some-other-random-key",
    })
  );
  app.use(flash());
  app.use(bodyparser.urlencoded({ extended: true }));

  // file upload
  app.use(
    fileupload({
      limits: {
        fileSize: 2 * 1024 * 1024 * 1024,
      },
      useTempFiles: true,
    })
  );

  return app;
};
