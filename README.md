# file-manager

A basic node.js file manager.

Forked and modified from https://github.com/serverwentdown/file-manager

modified to be used with [S]-Box from it's dashboard.

for documentation about the use with [S]-Box see: [internal faq page about Media Management](https://faq.spectitular.com/en/user-guide-panthea-live/media-managementt)

## Usage

For users who prefer Docker:

```zsh
docker run --rm -it -v $PWD:/data -p 8080:8080 spectitular/file-manager
```

Or if you have Node.js installed:

```zsh
npm install -g https://gitlab.com/spectitular/file-manager.git
file-manager
```


## Features

- [x] Simple authentication
- [x] Directory browsing
  - [x] Filesize
  - [ ] Permissions
  - [ ] Owner
- [x] Folder creation
- [x] File uploads
  - [x] Bulk file uploads
  - [ ] Large file uploads (sharded)
- [x] File/folder renaming
- [x] Previews for small image files
- [x] Bulk file/folder selection
  - [x] Delete
    - [x] Recursive directory delete
  - [ ] Move
  - [x] Rename
  - [ ] Copy
  - [x] Download archive
  - [ ] Change permissions
- [x] Remote commands
- [x] Mark directory as sync directories for [S]-Live
- [x] serve `/syncMedia.json` with sync info for [S]-Live
  - [x] include hashes of all projects
  - [x] include hashes of all subfolders
  - [x] cashes `syncMedia.json` until change in filetree.

### Sync Features for [S]-Live Viewer

A new addition for the panthea-live environment, is the sync button that shows up next to folders on the 3rd level of the file-tree:

![](https://faq.spectitular.com/panthea-live/sbox-files.png)

Activating it sets a .sync-this-folder file as a flag inside the directory to be synced.

![](https://faq.spectitular.com/panthea-live/media-management/file-manager-video.png)

The file-manager also serves `/syncMedia.json` file listing all project media folders and their files. 

This indicates to [[S]-Live Viewer Apps using their syncMediaClient](https://faq.spectitular.com/en/panthea-live/media-management#h-4-send-devices-to-content-sync) that this folder should be downloaded

#### syncMedia.json
an example of syncMedia.json, including md5 hashes for different levels of the project media files can be found at [./syncMedia.json.example](./syncMedia.json.example)

## Screenshots

These screenshots are not up-to-date.

![](https://faq.spectitular.com/panthea-live/sbox-files.png)

![](https://faq.spectitular.com/panthea-live/sbox-upload-success.png)

![](https://faq.spectitular.com/panthea-live/sbox-file-manager-directory.png)

![](https://faq.spectitular.com/panthea-live/media-management/files-sync-on.png)

![](https://faq.spectitular.com/panthea-live/media-management/file-manager-video.png)

## Options

The following environmental variables can be used to configure `file-manager`.

### SESSION_KEY=

Express session key, generate something random.

### SHELL=

This has been removed in [SPECTITULAR] fork for security reasons

### CMD=

This has been removed in [SPECTITULAR] fork for security reasons

### PORT=

Listen on $PORT. Default: 8080

### KEY=

Setting this variable enables authentication using TOTP (RFC6238). $KEY is a base32 encoded shared secret. This key is only a weak means of protection as it is succeptable to brute-force. You can generate one from [here](http://www.xanxys.net/totp/) or manually.

### DIR=

Setting this variable changes the directory of the running process to find the `content` dir. This has been added for the development environment of [S] where we use a proxy from `localhost:64343/file-manager` to point us to the file-manager running on `localhost:8080`. Set it to the location of your local [S] content directory.
