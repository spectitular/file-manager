#!/usr/bin/env node

/* jshint esversion: 6 */
/* jshint node: true */
"use strict";

require("dotenv").config();
const os = require("os");
const express = require("express");
const router = express.Router();
const routes = require("./routes")(router, {});
const favicon = require("serve-favicon");
const { engine: hbs } = require("express-handlebars");
const bodyparser = require("body-parser");
const session = require("express-session");
const flash = require("connect-flash");
const querystring = require("querystring");
const process = require("process");

const archiver = require("archiver");

const fs = require("fs");
const rimraf = require("rimraf");
const path = require("path");
const checkDiskSpace = require('check-disk-space').default

const filesize = require("filesize");
const octicons = require("@primer/octicons");
const handlebars = require("handlebars");

const { Dropzone } = require("dropzone");
const crypto = require("crypto");

const port = process.env.PORT || 8080;
const app = express();
const http = app.listen(port);

const hash = require('object-hash');


let syncLevel = 2;
if (process.env.syncLevel !== undefined) {
  syncLevel = process.env.syncLevel;
}

console.log('ENV DIR', process.env.DIR);
if(process.env.DIR) {
  process.chdir(process.env.DIR);
}

//const fileupload = require("express-fileupload");
//const fastFolderSize = require("fast-folder-size");
const fastFolderSizeSync = require("fast-folder-size/sync");

const SYNC_THIS_FOLDER_FILENAME = ".sync-this-folder";
const acceptedFiletypes = [
  ".pdf",
  ".doc",
  ".docx",
  ".xls",
  ".xlsx",
  ".odt",
  ".rtf",
  ".spectitular",
  ".mp4",
  ".m4a",
  ".mp3",
  ".png",
  ".gif",
  ".zip",
  ".jpg",
  ".ttf",
  ".woff2",
  ".webm",
  ".ogg"
];

let hbsConfig = {
  partialsDir: path.join(__dirname, "views", "partials"),
  layoutsDir: path.join(__dirname, "views", "layouts"),
  defaultLayout: "main",
  helpers: {
    syncLevel: syncLevel,
    either: function (a, b, options) {
      if (a || b) {
        return options.fn(this);
      }
    },
    isSyncLevelDepth: function (depth, options) {
      if (depth == syncLevel) {
        return options.fn(this);
      } else {
        return options.inverse(this);
      }
    },
    base: "/file-manager",
    assetsPath: "/file-manager/@assets/",

    filesize: filesize,
    octicon: function (i, options) {
      if (!octicons[i]) {
        return new handlebars.SafeString(octicons.question.toSVG());
      }
      return new handlebars.SafeString(octicons[i].toSVG());
    },
    eachpath: function (path, options) {
      if (typeof path != "string") {
        return "";
      }
      let out = [];
      path = path.split("/");
      path.splice(path.length - 1, 1);
      path.unshift("");
      path.forEach((folder, index) => {
        if (!folder) {
          out.push(
            options.fn({
              name: "data",
              path: "",
              depth: index,
              current: index === path.length - 1,
            })
          );
        } else {
          out.push(
            options.fn({
              name: folder,
              path: "/" + path.slice(1, index + 1).join("/"),
              depth: index,
              current: index === path.length - 1,
            })
          );
        }
      });
      return out.join("");
    },
  },
};


console.log('ENV base', process.env.base);

if (process.env.base !== undefined) {
  hbsConfig.helpers.base = process.env.base;
  if(hbsConfig.helpers.base === '/') {
    hbsConfig.helpers.base = '';
  }
}


// runs in docker-container as a relative path  /file-manager
// this is used for local host debugging with .env assetsPath=/@assets/
if (process.env.assetsPath) {
  hbsConfig.helpers.assetsPath = process.env.assetsPath;
}

app.use("/", routes);

app.get("/info.json", async function (req, res) {
  res.setHeader("Content-Type", "application/json");
  const uptime = process.uptime();
  const started = Date.now() - uptime * 1000;

  let manifest = {
    version: "0.0.0.1",
    name: "unknown-product",
  };

  if (fs.existsSync("/usr/local/share/file-manager/package.json")) {
    manifest = require("/usr/local/share/file-manager/package.json");
  }

  if (fs.existsSync("./package.json")) {
    manifest = require("./package.json");
  }

  checkDiskSpace('/data').then((diskSpace) => {
    res.send(
      JSON.stringify({
        product: "[SPECTITULAR] " + manifest.name,
        uptime: Math.round(uptime * 100) / 100 + " sec",
        "x-forwarded-host": req.headers["x-forwarded-host"],
        "X-Forwarded-For": req.headers["X-Forwarded-For"],
        hostname: req.hostname,
        diskSpace: diskSpace,
        hash: os.hostname(),
        base: hbsConfig.helpers.base,
        version: manifest.version,
        started: new Date(started).toLocaleString(),
      })
    );
  });

});

app.set("views", path.join(__dirname, "views"));
app.engine("handlebars", hbs(hbsConfig));
app.set("view engine", "handlebars");

// AUTH
const KEY = false;

app.get("/@logout", (req, res) => {
  if (KEY) {
    req.session.login = false;
    req.flash("success", "Signed out.");
    res.redirect("/@login");
    return;
  }
  req.flash("error", "You are not logged in...");
  res.redirect("back");
});

app.get("/@login", (req, res) => {
  res.render("login", flashify(req, {}));
});

app.post("/@login", (req, res) => {
  let pass = true;
  if (pass) {
    req.session.login = true;
    res.redirect("/");
    return;
  }
  req.flash("error", "Bad token.");
  res.redirect("/@login");
});

app.use((req, res, next) => {
  if (!KEY) {
    return next();
  }
  if (req.session.login === true) {
    return next();
  }
  req.flash("error", "Please sign in.");
  res.redirect("/@login");
});

function relative(...paths) {
  const finalPath = paths.reduce((a, b) => path.join(a, b), process.cwd());
  if (path.relative(process.cwd(), finalPath).startsWith("..")) {
    throw new Error("Failed to resolve path outside of the working directory");
  }
  return finalPath;
}

function flashify(req, obj) {
  let error = req.flash("error");
  if (error && error.length > 0) {
    if (!obj.errors) {
      obj.errors = [];
    }
    obj.errors.push(error);
  }
  let success = req.flash("success");
  if (success && success.length > 0) {
    if (!obj.successes) {
      obj.successes = [];
    }
    obj.successes.push(success);
  }
  obj.isloginenabled = !!KEY;
  return obj;
}

app.use((req, res, next) => {
  if (req.method === "GET") {
    return next();
  }
  let sourceHost = null;
  if (req.headers.origin) {
    sourceHost = new URL(req.headers.origin).host;
  } else if (req.headers.referer) {
    sourceHost = new URL(req.headers.referer).host;
  }
  //console.log(':::', req.headers);
  //console.log(':::', sourceHost, req.headers.host);
  if (sourceHost !== req.headers.host && req.headers.host !== 'localhost:8080') {
    throw new Error(
      "Origin or Referer header does not match or is missing. Request has been blocked to prevent CSRF"
    );
  }
  next();
});

app.all("/*", (req, res, next) => {
  res.filename = req.params[0];

  if(res.filename.includes('list.json')) {
    req.mode = 'list';
    res.filename = req.params[0].replace('list.json','');
  }

  let fileExists = new Promise((resolve, reject) => {
    // check if file exists
    fs.stat(relative(res.filename), (err, stats) => {
      if (err) {
        return reject(err);
      }
      return resolve(stats);
    });
  });

  fileExists
    .then((stats) => {
      res.stats = stats;
      next();
    })
    .catch((err) => {
      res.stats = { error: err };
      next();
    });
});

app.post("/*@upload", (req, res) => {
  let responseObj = new Array();
  res.filename = req.params[0];

  if (req.files) {
    let arr;

    if (Array.isArray(req.files.file)) {
      arr = req.files.file;
    } else {
      arr = [req.files.file];
    }

    for (let i = 0; i < arr.length; i++) {
      let file = arr[i];

      if (acceptedFiletypes.indexOf(path.extname(file.name)) > -1) {
        console.log("file.name",file.name);
        file.name = file.name.replace(/ /g, '_')
        console.log("file.name after replace",file.name);
        const filenameOutput = relative(res.filename, file.name);
        let sameFile = false;
        if (fs.existsSync(filenameOutput)) {
          //console.log("FILE EXISTS");
          let fileData = fs.readFileSync(filenameOutput);
          let md5 = crypto.createHash("md5").update(fileData).digest("hex");
          //console.log("EXISTING md5", md5);
          //console.log("new md5", file.md5);

          if (md5 === file.md5) {
            sameFile = true;
          }
        }

        if (!sameFile) {
          file.mv(filenameOutput, function (err) {
            if (err) {
              //console.log(err);
            }
          });
          responseObj.push(filenameOutput);
          cache.forceUpdate = true;
        } else {
          responseObj = {
            error: "File already exists (name and md5)",
          };
        }
      } else {
        responseObj = {
          error: "This filetype " + file.mimetype + " is not supported",
        };
      }
    }
  }
  // give the server a second to write the files

  if (responseObj.length === 0) {
    responseObj = {
      error: "file could not be saved",
    };
  }

  if (responseObj.error) {
    res.status(500);
    res.json(responseObj);
  } else {
    setTimeout(function () {
      res.json(responseObj);
    }, 1000);
  }
});

app.get("/syncMedia.json", (req,res) => {
  return serveMediaList(req,res, { filtered: false });
});
app.get("/syncMediaFiltered.json", (req,res) => {
  return serveMediaList(req,res, { filtered: true });
});
app.get("/cacheFlush.json", (req, res) => {
      cache.forceUpdate = true;
      res.json(cache);
});


const cache = {
  lastUpdate: Date.now(),
  forceUpdate: false,
  isUsed: false
}

async function getMediaListCached(hostPrefix) {

  let projects = [];
  // 5min
  const MAX_CACHE_AGE = 5*60*1000;
  let cacheAge = Date.now() - cache.lastUpdate

  const CACHE_OUTDATED = cache.lastUpdate && cacheAge > MAX_CACHE_AGE
  if(cache.projects && !CACHE_OUTDATED && !cache.forceUpdate) {
    projects = cache.projects;
    cache.isUsed = true;
  } else {
    projects = await getMediaList(hostPrefix);
    cache.projects = projects;
    cache.lastUpdate = Date.now();
    cache.forceUpdate = false;
    cache.isUsed = false;
    cacheAge = 0;
  }

  return {
    projects,
    cacheAge
  };

}

async function serveMediaList(req, res, opt) {

  let hostPrefix = req.protocol + "://" + req.get("host");
  // fix for local development
  /*
  if(hostPrefix === 'http://localhost:8080') {

    function getLocalIPs(){
      const { networkInterfaces } = require('os');
      const nets = networkInterfaces();
      const results = Object.create({});

      for (const name of Object.keys(nets)) {
          for (const net of nets[name]) {
              const familyV4Value = typeof net.family === 'string' ? 'IPv4' : 4
              if (net.family === familyV4Value && !net.internal) {
                  if (!results[name]) {
                      results[name] = [];
                  }
                  results[name].push(net.address);
              }
          }
      }
      return results
    }
    // identify network address dynamically:
    const localIps = getLocalIPs();
    console.log("localIps for development:",localIps);

    // do not use cache for localhost development
    cache.forceUpdate = true;

    hostPrefix = "http://"+ (localIps.eth0?.[0] || localIps.en0[0])+":64343"
  }
  */

  const list = await getMediaListCached(hostPrefix);
  let { projects } = list;

  if(opt.filtered) {
    projects = projects.map(project => {
      return {
        name: project.name,
        hashedProjectName: project.hashedProjectName,
        totalFiles: project.totalFiles,
        hash: project.hash
      }
    })
  }

  res.setHeader("Access-Control-Allow-Origin", "*");
  res.setHeader("Content-Type", "application/json");
  res.json({
    cached: cache.isUsed,
    cacheAge: list.cacheAge,
    hash: md5(getProjectsString(projects)),
    projects
  });
}

async function getMediaList(hostPrefix) {
  let projects = [];

  let readDir = new Promise((resolve, reject) => {
    fs.readdir(relative("/"), { withFileTypes: true }, (err, filenames) => {
      if (err) {
        return reject(err);
      }
      return resolve(filenames);
    });
  });

  return readDir.then(async (projectNames) => {
    const projectsToSync = [];

    for (const file of projectNames) {
      if (file.isDirectory()) {
        //console.log(file);
        const syncFolders = await checkForSyncFile(file.name);
        if (syncFolders.length > 0 && file.name[0] !=='.') {
          //console.log('!!!!!', file.name);
          //console.log(syncFolders);
          projectsToSync.push({
            name: file.name,
            syncFolders: syncFolders
          });
        }
      }
    }


    for (const project of projectsToSync) {

      //console.log('project', project);

      const fileList = await getAssetsFileList(
        hostPrefix,
        project.name,
        relative(project.name)
      );

      if (fileList.length > 0) {

        const filteredFileList = fileList.filter(file => {
          if (file.isDirectory) return // ignore directories

          if(file.name[0] === '.') return // ignore . files

          const fullFilePath = path.join(relative("/") , project.name, file.path)+"/";
          // console.log('SYNC FOLDER', project.syncFolders);
          // console.log('fullFilePath', fullFilePath);
          const doesInclude = project.syncFolders.some(sf => fullFilePath.includes(sf+"/"));

          //  console.log('doesInclude', doesInclude);

          return doesInclude
        })

        projects.push({
          name: project.name,
          hashedProjectName: hash(project.name),
          hash: md5(getFileString(filteredFileList)),
          folders: {},
          files: filteredFileList,
          totalFiles: filteredFileList.length
        });
      }
    }

    //now add a folders with hashes on folder level
    projects.forEach((project) => {
      
      for (let file of project.files) {
        if(!project['folders'][file.path]) {
          project['folders'][file.path] = {
            hash: '',
            files: []
          }
        }
        const current = project['folders'][file.path];
        current.files = current.files.concat(file);
      }

      Object.values(project['folders']).forEach(folderObj => {
        folderObj.hash = md5(getFileString(folderObj.files))
      });

    })

    return projects;


  });
}


function getFileString(fileList) {
  let string = '';
  fileList.sort((a,b) => a.name.localeCompare(b.name));
  fileList.forEach(file => {
    string += file.path + '/';
    string += file.name;
    string += file.size
  })
  return string;
}

function getProjectsString(projects) {
  let string = '';
  projects.sort();
  projects.forEach(project => {
    string += project.name;
    string += project.hash
  })
  return string;
}

/*
function getHashForFiles(fileList) {
  const string = getFileString(fileList)
  return crypto.createHash('md5').update(string).digest('hex')
}

function getHashForFiles2(fileList) {
  const string = getFileString(fileList)
  return md5(string);
}
*/


// https://stackoverflow.com/questions/1655769/fastest-md5-implementation-in-javascript
//  A formatted version of a popular md5 implementation.
//  Original copyright (c) Paul Johnston & Greg Holt.
//  The function itself is now 42 lines long.

function md5(inputString) {
  var hc="0123456789abcdef";
  function rh(n) {var j,s="";for(j=0;j<=3;j++) s+=hc.charAt((n>>(j*8+4))&0x0F)+hc.charAt((n>>(j*8))&0x0F);return s;}
  function ad(x,y) {var l=(x&0xFFFF)+(y&0xFFFF);var m=(x>>16)+(y>>16)+(l>>16);return (m<<16)|(l&0xFFFF);}
  function rl(n,c)            {return (n<<c)|(n>>>(32-c));}
  function cm(q,a,b,x,s,t)    {return ad(rl(ad(ad(a,q),ad(x,t)),s),b);}
  function ff(a,b,c,d,x,s,t)  {return cm((b&c)|((~b)&d),a,b,x,s,t);}
  function gg(a,b,c,d,x,s,t)  {return cm((b&d)|(c&(~d)),a,b,x,s,t);}
  function hh(a,b,c,d,x,s,t)  {return cm(b^c^d,a,b,x,s,t);}
  function ii(a,b,c,d,x,s,t)  {return cm(c^(b|(~d)),a,b,x,s,t);}
  function sb(x) {
    var i;var nblk=((x.length+8)>>6)+1;var blks=new Array(nblk*16);for(i=0;i<nblk*16;i++) blks[i]=0;
    for(i=0;i<x.length;i++) blks[i>>2]|=x.charCodeAt(i)<<((i%4)*8);
    blks[i>>2]|=0x80<<((i%4)*8);blks[nblk*16-2]=x.length*8;return blks;
  }
  var i,x=sb(inputString),a=1732584193,b=-271733879,c=-1732584194,d=271733878,olda,oldb,oldc,oldd;
  for(i=0;i<x.length;i+=16) {olda=a;oldb=b;oldc=c;oldd=d;
    a=ff(a,b,c,d,x[i+ 0], 7, -680876936);d=ff(d,a,b,c,x[i+ 1],12, -389564586);c=ff(c,d,a,b,x[i+ 2],17,  606105819);
    b=ff(b,c,d,a,x[i+ 3],22,-1044525330);a=ff(a,b,c,d,x[i+ 4], 7, -176418897);d=ff(d,a,b,c,x[i+ 5],12, 1200080426);
    c=ff(c,d,a,b,x[i+ 6],17,-1473231341);b=ff(b,c,d,a,x[i+ 7],22,  -45705983);a=ff(a,b,c,d,x[i+ 8], 7, 1770035416);
    d=ff(d,a,b,c,x[i+ 9],12,-1958414417);c=ff(c,d,a,b,x[i+10],17,     -42063);b=ff(b,c,d,a,x[i+11],22,-1990404162);
    a=ff(a,b,c,d,x[i+12], 7, 1804603682);d=ff(d,a,b,c,x[i+13],12,  -40341101);c=ff(c,d,a,b,x[i+14],17,-1502002290);
    b=ff(b,c,d,a,x[i+15],22, 1236535329);a=gg(a,b,c,d,x[i+ 1], 5, -165796510);d=gg(d,a,b,c,x[i+ 6], 9,-1069501632);
    c=gg(c,d,a,b,x[i+11],14,  643717713);b=gg(b,c,d,a,x[i+ 0],20, -373897302);a=gg(a,b,c,d,x[i+ 5], 5, -701558691);
    d=gg(d,a,b,c,x[i+10], 9,   38016083);c=gg(c,d,a,b,x[i+15],14, -660478335);b=gg(b,c,d,a,x[i+ 4],20, -405537848);
    a=gg(a,b,c,d,x[i+ 9], 5,  568446438);d=gg(d,a,b,c,x[i+14], 9,-1019803690);c=gg(c,d,a,b,x[i+ 3],14, -187363961);
    b=gg(b,c,d,a,x[i+ 8],20, 1163531501);a=gg(a,b,c,d,x[i+13], 5,-1444681467);d=gg(d,a,b,c,x[i+ 2], 9,  -51403784);
    c=gg(c,d,a,b,x[i+ 7],14, 1735328473);b=gg(b,c,d,a,x[i+12],20,-1926607734);a=hh(a,b,c,d,x[i+ 5], 4,    -378558);
    d=hh(d,a,b,c,x[i+ 8],11,-2022574463);c=hh(c,d,a,b,x[i+11],16, 1839030562);b=hh(b,c,d,a,x[i+14],23,  -35309556);
    a=hh(a,b,c,d,x[i+ 1], 4,-1530992060);d=hh(d,a,b,c,x[i+ 4],11, 1272893353);c=hh(c,d,a,b,x[i+ 7],16, -155497632);
    b=hh(b,c,d,a,x[i+10],23,-1094730640);a=hh(a,b,c,d,x[i+13], 4,  681279174);d=hh(d,a,b,c,x[i+ 0],11, -358537222);
    c=hh(c,d,a,b,x[i+ 3],16, -722521979);b=hh(b,c,d,a,x[i+ 6],23,   76029189);a=hh(a,b,c,d,x[i+ 9], 4, -640364487);
    d=hh(d,a,b,c,x[i+12],11, -421815835);c=hh(c,d,a,b,x[i+15],16,  530742520);b=hh(b,c,d,a,x[i+ 2],23, -995338651);
    a=ii(a,b,c,d,x[i+ 0], 6, -198630844);d=ii(d,a,b,c,x[i+ 7],10, 1126891415);c=ii(c,d,a,b,x[i+14],15,-1416354905);
    b=ii(b,c,d,a,x[i+ 5],21,  -57434055);a=ii(a,b,c,d,x[i+12], 6, 1700485571);d=ii(d,a,b,c,x[i+ 3],10,-1894986606);
    c=ii(c,d,a,b,x[i+10],15,   -1051523);b=ii(b,c,d,a,x[i+ 1],21,-2054922799);a=ii(a,b,c,d,x[i+ 8], 6, 1873313359);
    d=ii(d,a,b,c,x[i+15],10,  -30611744);c=ii(c,d,a,b,x[i+ 6],15,-1560198380);b=ii(b,c,d,a,x[i+13],21, 1309151649);
    a=ii(a,b,c,d,x[i+ 4], 6, -145523070);d=ii(d,a,b,c,x[i+11],10,-1120210379);c=ii(c,d,a,b,x[i+ 2],15,  718787259);
    b=ii(b,c,d,a,x[i+ 9],21, -343485551);a=ad(a,olda);b=ad(b,oldb);c=ad(c,oldc);d=ad(d,oldd);
  }
  return rh(a)+rh(b)+rh(c)+rh(d);
}



async function checkForSyncFile(dirPath) {
  const checkFolder = path.join(dirPath);
  // content/PROJECT1/files/this_is_a_sync_folder/.sync-this-folder
  const allFiles = await getFilesRecursive(checkFolder, 0, 3);

  let syncThisFolders = [];
  if(Array.isArray(allFiles)) {
    syncThisFolders = allFiles.filter(file => file.basename === SYNC_THIS_FOLDER_FILENAME).map(file => path.dirname(file.path))
  }
  //console.log('!:!:!', dirPath, syncThisFolders);
  return syncThisFolders;
}

async function getAssetsFileList(hostPrefix, projectName, dirPath) {
  let fileList = [];

  const files = await getFilesRecursive(dirPath, 0, 4);

  if (files.length === 0 || files.isEmpty) {
    return fileList;
  }

  files.forEach((dirEnt) => {
    let relativePathOS = path.relative(dirPath, dirEnt.path);
    let relativeDirs = path.parse(relativePathOS).dir.split(path.sep);
    let relativePathWithSlash = "";

    // array has [ '' ] for 1st level files
    if (relativeDirs.length > 0 && relativeDirs[0] !== "") {
      relativePathWithSlash = "/" + relativeDirs.join("/");
    }

    let file = {
      relativePath: relativePathWithSlash,
      name: dirEnt.basename,
    };
    let extName = dirEnt.ext;
    try {
      file.stat = fs.statSync(dirEnt.path);
    } catch (e) {
      //console.log(e);
    }

    if (dirEnt.isDirectory) {
      fileList.push({
        isDirectory: true,
        path: file.relativePath,
        name: file.name,
        lastModified: file.time,
        size: file.stat.size,
        filesize: filesize(file.stat.size),
      });
    }

    if (acceptedFiletypes.indexOf(extName) > -1) {
      fileList.push({
        path: file.relativePath,
        name: file.name,
        download: encodeURI(
          hostPrefix + hbsConfig.helpers.base  + '/' + projectName + file.relativePath + "/" + file.name
        ),
        lastModified: file.time,
        size: file.stat.size,
        filesize: filesize(file.stat.size),
      });
    }
  });

  return fileList;
}

async function getFilesRecursive(dir, level = 0,maxDepth = 3) {
  let error = false;
  await fs.promises.access(dir).catch((e) => {
    //console.log(e);
    if (e.code == "ENOENT") {
      error = true;
      return [];
    }
  });
  if (error) {
    return ["error"];
  }
  const dirents = await fs.promises.readdir(dir, { withFileTypes: true });
  if (dirents.length === 0) {
    return {
      path: dir,
      isDirectory: true,
      isEmpty: true,
      basename: path.basename(dir),
    };
  }
  const files = await Promise.all(
    dirents.map((dirent) => {
      const res = path.resolve(dir, dirent.name);
      if(dirent.isDirectory()) {
        if(level < maxDepth) {
          return getFilesRecursive(res, level +1, maxDepth)
        } else {
          return {
            path: res,
            isDirectory: true,
            isOutsideOfMaxDepth: true,
            basename: path.basename(res),
            ext: path.extname(res),
          }
        }
      } else {
        return {
          path: res,
          isDirectory: false,
          basename: path.basename(res),
          ext: path.extname(res),
        }
      }
    })
  );
  return Array.prototype.concat(...files);
}

app.post("/*@mkdir", (req, res) => {
  res.filename = req.params[0];

  let folder = req.body.folder;
  if (!folder || folder.length < 1) {
    return res.status(400).end();
  }

  let fileExists = new Promise((resolve, reject) => {
    // Check if file exists
    fs.stat(relative(res.filename, folder), (err, stats) => {
      if (err) {
        return reject(err);
      }
      return resolve(stats);
    });
  });

  fileExists
    .then((stats) => {
      req.flash("error", "Folder exists, cannot overwrite. ");
      res.redirect("back");
    })
    .catch((err) => {
      const mkDirPath = relative(res.filename, folder);
      //console.log(mkDirPath);
      fs.mkdir(mkDirPath, (err) => {
        if (err) {
          console.warn(err);
          req.flash("error", err.toString());
          res.redirect("back");
          return;
        }
        cache.forceUpdate = true;
        req.flash("success", "Folder " + folder + " created. ");
        res.redirect("back");
      });
    });
});

app.post("/*@delete", (req, res) => {
  res.filename = req.params[0];

  let files = JSON.parse(req.body.files);
  if (!files || !files.map || (files && files.length === 0)) {
    req.flash("error", "No files selected.");
    res.redirect("back");
    return; // res.status(400).end();
  }

  let promises = files.map((f) => {
    return new Promise((resolve, reject) => {
      fs.stat(relative(res.filename, f), (err, stats) => {
        if (err) {
          return reject(err);
        }
        resolve({
          name: f,
          isdirectory: stats.isDirectory(),
          isfile: stats.isFile(),
        });
      });
    });
  });
  Promise.all(promises)
    .then((files) => {
      let promises = files.map((f) => {
        return new Promise((resolve, reject) => {
          let op = null;
          if (f.isdirectory) {
            op = (dir, cb) =>
              rimraf(
                dir,
                {
                  glob: false,
                },
                cb
              );
          } else if (f.isfile) {
            op = fs.unlink;
          }
          if (op) {
            op(relative(res.filename, f.name), (err) => {
              if (err) {
                return reject(err);
              }
              resolve();
            });
          }
        });
      });
      Promise.all(promises)
        .then(() => {
          cache.forceUpdate = true;
          req.flash(
            "success",
            files.length === 1
              ? "1 File deleted"
              : files.length + " Files deleted."
          );
          res.redirect("back");
        })
        .catch((err) => {
          console.warn(err);
          req.flash("error", "Unable to delete some files: " + err);
          res.redirect("back");
        });
    })
    .catch((err) => {
      console.warn(err);
      req.flash("error", err.toString());
      res.redirect("back");
    });
});

app.get("/*@toggleSync", (req, res) => {
  const filename = req.params[0];
  const syncFile = relative(filename, SYNC_THIS_FOLDER_FILENAME);

  if (fs.existsSync(syncFile)) {
    fs.unlink(syncFile, function (err, stats) {
      cache.forceUpdate = true;
      req.flash("success", filename + " removed from sync.");
      res.redirect("back");
    });
  } else {
    fs.closeSync(fs.openSync(syncFile, "w"));
    cache.forceUpdate = true;
    req.flash("success", filename + " added to sync.");
    res.redirect("back");
  }
});

app.post("/*@download", (req, res) => {
  res.filename = req.params[0];
  const zipname = req.body.zipname;
  let files = null;
  try {
    files = JSON.parse(req.body.files);
  } catch (e) {}

  //console.log('files', files);
  //console.log('zipname', zipname);

  if (!files || !files.map) {
    req.flash("error", "No files selected.");
    res.redirect("back");
    return; // res.status(400).end();
  }

  let promises = files.map((f) => {
    return new Promise((resolve, reject) => {
      fs.stat(relative(res.filename, f), (err, stats) => {
        if (err) {
          return reject(err);
        }
        resolve({
          name: f,
          isdirectory: stats.isDirectory(),
          isfile: stats.isFile(),
        });
      });
    });
  });
  Promise.all(promises)
    .then((files) => {
      let zip = archiver("zip", {});
      zip.on("error", function (err) {
        console.warn(err);
        res.status(500).send({
          error: err.message,
        });
      });

      files
        .filter((f) => f.isfile)
        .forEach((f) => {
          zip.file(relative(res.filename, f.name), { name: f.name });
        });
      files
        .filter((f) => f.isdirectory)
        .forEach((f) => {
          zip.directory(relative(res.filename, f.name), f.name);
        });

      res.attachment(zipname);
      zip.pipe(res);

      zip.finalize();
    })
    .catch((err) => {
      console.warn(err);
      req.flash("error", err.toString());
      res.redirect("back");
    });
});

app.post("/*@rename", (req, res) => {
  res.filename = req.params[0];

  if (!req.body.files) {
    req.flash("error", "No files selected...");
    res.redirect("back");
    return;
  }

  let files = JSON.parse(req.body.files);

  // PROBABLY NOT NEEDED
  if (!files || !files.map || (files && files.length === 0)) {
    req.flash("error", "No files selected.");
    res.redirect("back");
    return;
  }

  new Promise((resolve, reject) => {
    fs.access(relative(res.filename), fs.constants.W_OK, (err) => {
      if (err) {
        return reject(err);
      }
      resolve();
    });
  })
    .then(() => {
      let promises = files.map((f) => {
        return new Promise((resolve, reject) => {
          fs.rename(
            relative(res.filename, f.original),
            relative(res.filename, f.new),
            (err) => {
              if (err) {
                return reject(err);
              }
              resolve();
            }
          );
        });
      });
      Promise.all(promises)
        .then(() => {
          cache.forceUpdate = true;
          req.flash("success", files.length + " files renamed. ");
          res.redirect("back");
        })
        .catch((err) => {
          console.warn(err);
          req.flash("error", "Unable to rename some files: " + err);
          res.redirect("back");
        });
    })
    .catch((err) => {
      console.warn(err);
      req.flash("error", err.toString());
      res.redirect("back");
    });
});

const SMALL_IMAGE_MAX_SIZE = 750 * 1024; // 750 KB
const EXT_IMAGES = [".jpg", ".jpeg", ".png", ".webp", ".svg", ".gif", ".tiff"];
function isimage(f) {
  for (const ext of EXT_IMAGES) {
    if (f.endsWith(ext)) {
      return true;
    }
  }
  return false;
}

app.get("/*", (req, res) => {
  if (res.stats.error) {
    res.render(
      "list",
      flashify(req, {
        path: res.filename,
        errors: [res.stats.error],
      })
    );
  } else if (res.stats.isDirectory()) {
    if (!req.url.endsWith("/") && req.mode !== 'list') {
      return res.redirect(req.url + "/");
    }

    let readDir = new Promise((resolve, reject) => {
      fs.readdir(relative(res.filename), (err, filenames) => {
        if (err) {
          return reject(err);
        }
        return resolve(filenames);
      });
    });

    readDir
      .then((filenames) => {
        const promises = filenames.map(
          (f) =>
            new Promise((resolve, reject) => {
              fs.stat(relative(res.filename, f), (err, stats) => {
                if (err) {
                  console.warn(err);
                  return resolve({
                    name: f,
                    error: err,
                  });
                }
                let syncThisFolder = false;
                let size = stats.size;
                if (stats.isDirectory()) {
                  syncThisFolder = fs.existsSync(
                    relative(res.filename, f, SYNC_THIS_FOLDER_FILENAME)
                  );
                  size = fastFolderSizeSync(relative(res.filename, f));
                }

                const segments = res.filename.split('/');
                const depth = segments.length - 1;
                let fullpath = res.filename + f;

                if(depth > 0) {
                  segments.shift()
                  fullpath = segments.join('/') + f
                }

                resolve({
                  name: f,
                  fullpath,
                  dotfile: f[0] === ".",
                  isdirectory: stats.isDirectory(),
                  depth,
                  syncThisFolder: syncThisFolder,
                  issmallimage: isimage(f) && stats.size < SMALL_IMAGE_MAX_SIZE,
                  size: size,
                });
              });
            })
        );

        Promise.all(promises)
          .then((files) => {

            if(req.mode === 'list') {
              res.json(files);
            } else {
              res.render(
                "list",
                flashify(req, {
                  path: res.filename,
                  files: files,
                })
              );
            }
          })
          .catch((err) => {
            console.error(err);
            res.render(
              "list",
              flashify(req, {
                path: res.filename,
                errors: [err],
              })
            );
          });
      })
      .catch((err) => {
        console.warn(err);
        res.render(
          "list",
          flashify(req, {
            path: res.filename,
            errors: [err],
          })
        );
      });
  } else if (res.stats.isFile()) {
    res.sendFile(relative(res.filename), {
      headers: {
        "Content-Security-Policy":
          "default-src 'self'; script-src 'none'; sandbox",
      },
      dotfiles: "allow",
    });
  }
});

console.log(`Listening on http://localhost:${port}`);
